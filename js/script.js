const filmsDIV = document.querySelector('#films');

let films;
let characters;

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(filmsData => {
    films = filmsData;

    films.forEach(film => {
      const filmElement = document.createElement('li');
      filmElement.id = `${film.id}`;
      filmElement.textContent = `Епізод ${film.episodeId}. ${film.name}. ${film.openingCrawl}`;
      filmsDIV.appendChild(filmElement);
    });

    getCharactersForEachFilm(films);
  });

function getCharactersForEachFilm(films) {
  films.forEach(film => {
    console.log(film);
    const charactersList = film.characters;

    const filmElement = document.getElementById(`${film.id}`);
    const charactersUL = document.createElement('ul');

    charactersList.forEach(characterLink => {
      fetch(characterLink)
      .then(response => response.json())
      .then(character => {
        console.log(character);
          const characterElement = document.createElement('li');
          characterElement.textContent = character.name;

          charactersUL.appendChild(characterElement);
      })
      filmElement.appendChild(charactersUL);
    })
  
  //   fetch(`https://ajax.test-danit.com/api/swapi/films`)
  //     .then(response => response.json())
  //     .then(charactersData => {
  //       characters = charactersData;

  //       const filmElement = document.querySelector(`ul li:contains('${film.name}')`);
  //       const charactersList = document.createElement('ul');

  //       charactersData.results.forEach(character => {
  //         const characterElement = document.createElement('li');
  //         characterElement.textContent = character.name;
  //         charactersList.appendChild(characterElement);
  //       });
  //       filmElement.appendChild(charactersList);
  //     });
});
}
